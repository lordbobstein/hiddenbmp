/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hiddenbmp.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 *
 * @author Lord Bobstein
 */
public class ImageComponent extends JComponent
{
    private final String path;
    private final Image image;

    public ImageComponent(String path) throws IOException
    {
        this.path = path;
        this.setPreferredSize(new Dimension(475, 580));
        
        image = ImageIO.read(new File(path));
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        g.drawImage(image,0,0, 475, 580, null);
    }
}

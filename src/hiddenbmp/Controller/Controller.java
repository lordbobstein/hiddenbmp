/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.Controller;

import hiddenbmp.model.BinDecrypter;
import hiddenbmp.model.BinEncrypter;
import hiddenbmp.model.BinTester;
import hiddenbmp.model.DataHeader;
import hiddenbmp.view.ImageComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import hiddenbmp.view.MainWindow;
import java.io.FileOutputStream;

/**
 *
 * @author Lord Bobstein
 */
public class Controller
{

    private final MainWindow window;
    private BinTester tester;
    private BinEncrypter encrypter;
    private BinDecrypter decrypter;
    private DataHeader dataHeader;

    private String decrypterFileExtension;

    public Controller(MainWindow window)
    {
        this.window = window;

        //////////////////////////////////////////////////////////////////////
        window.bKWyborPliku.addActionListener(new BKWyborPlikuListener());
        window.bKPrzegladajIn.addActionListener(new BKPrzegladajIn());
        window.bKWyborNowegoPliku.addActionListener(new BKWyborNowegoPlikuListener());
        window.bKSprawdz.addActionListener(new BKSprawdzListener());
        window.bKPodglad.addActionListener(new BKPodgladListener());
        window.bKKoduj.addActionListener(new BKKodujListener());

        window.bDPrzegladajPlikWejsciowy.addActionListener(new BDPrzegladajPlikWejsciowyListener());
        window.bDPrzegladajOut.addActionListener(new BDPrzegladajOutListener());
        window.bDSprawdz.addActionListener(new BDSprawdzListener());
        window.bDDekoduj.addActionListener(new BDDekodujListener());
    }

    class BKWyborPlikuListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent ae)
        {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("."));
            chooser.setFileFilter(new FileNameExtensionFilter("Pliki bmp", "bmp"));
            chooser.setAcceptAllFileFilterUsed(false); //wylaczenie wszystkich plikow
            int result = chooser.showDialog(window, "Wybierz plik");

            if (result == JFileChooser.APPROVE_OPTION)
            {
                window.tKSciezkaDoKodowania.setText(chooser.getSelectedFile().getPath());
            }

        }
    }

    class BKWyborNowegoPlikuListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent ae)
        {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("."));
            chooser.setFileFilter(new FileNameExtensionFilter("Pliki bmp", "bmp"));
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setAcceptAllFileFilterUsed(false); //wylaczenie wszystkich plikow
            int result = chooser.showSaveDialog(window);

            if (result == JFileChooser.APPROVE_OPTION)
            {
                window.tKSciezkaDoKodowaniaNowego.setText(chooser.getSelectedFile().getPath());
                window.tKNazwaNowegoPliku.setText("zakodowany.bmp");
            }
        }

    }

    class BKPrzegladajIn implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent ae)
        {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("."));
            chooser.setAcceptAllFileFilterUsed(true);
            int result = chooser.showDialog(window, "Wybierz plik");

            if (result == JFileChooser.APPROVE_OPTION)
            {
                window.tKSciezkaIn.setText(chooser.getSelectedFile().getPath());
            }
        }
    }

    class BKSprawdzListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent ae)
        {
            try
            {
                byte[] data;

                String plikTloPath = window.tKSciezkaDoKodowania.getText();

                if (window.rKBin.isSelected())
                {
                    String plikUkrywanyPath = window.tKSciezkaIn.getText();
                    FileInputStream plikUkrywany = new FileInputStream(new File(plikUkrywanyPath));

                    data = new byte[plikUkrywany.available()];
                    plikUkrywany.read(data);
                    plikUkrywany.close();

                }
                else
                {
                    data = window.tKText.getText().getBytes();
                }
                int[] c = getColorsBits();
                tester = new BinTester(data, plikTloPath, c[0], c[1], c[2]);

                boolean possible = tester.isEncryptionPossible();

                if (possible)
                {
                    window.tKCzyMoznaKodowac.setText("Tak");
                    System.out.println(((int) tester.getMaxDataLength()) - data.length);
                    window.addChart("temp.bmp", (int) tester.getMaxDataLength() - data.length, data.length);
                }
                else
                {
                    window.tKCzyMoznaKodowac.setText("Nie");
                    window.removeChart();
                }

            }
            catch (IOException io)
            {
                JOptionPane.showMessageDialog(window, "Błąd otwierania plików");
            }

        }

    }

    class BKPodgladListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent ae)
        {
            try
            {

                byte[] data;
                int[] c = getColorsBits();

                String imageBeforePath = window.tKSciezkaDoKodowania.getText();
                ImageComponent imageBefore = new ImageComponent(imageBeforePath);
                window.tabObrazekPrzed.removeAll();
                window.tabObrazekPrzed.add(imageBefore);
                window.tabObrazekPrzed.repaint();

                String backgroundFilePath = window.tKSciezkaDoKodowania.getText();
                FileInputStream backgroundFile = new FileInputStream(new File(backgroundFilePath));

                String outFilePath = "temp.bmp";
                FileOutputStream outFile = new FileOutputStream(new File(outFilePath));

                if (window.rKBin.isSelected())
                {
                    String plikUkrywanyPath = window.tKSciezkaIn.getText();
                    FileInputStream hiddenFile = new FileInputStream(new File(plikUkrywanyPath));

                    data = new byte[hiddenFile.available()];
                    hiddenFile.read(data);
                    hiddenFile.close();

                }
                else
                {
                    data = window.tKText.getText().getBytes();
                }

                encrypter = new BinEncrypter(data, "test", backgroundFilePath, outFilePath, c[0], c[1], c[2]);
                encrypter.encryptData();

                String imageAfterPath = outFilePath;
                ImageComponent imageAfter = new ImageComponent(imageAfterPath);

                window.tabObrazekPo.removeAll();
                window.tabObrazekPo.add(imageAfter);
                window.tabObrazekPo.repaint();

                window.addChart("temp.bmp", (int) encrypter.getMaxDataLength() - data.length, data.length);
                JOptionPane.showMessageDialog(window, "Wygenerowano podgląd.");
            }
            catch (IOException ex)
            {
                JOptionPane.showMessageDialog(window, "Błąd generowania podglądu.");
            }
        }
    }

    class BKKodujListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent ae)
        {
            try
            {
                byte[] data;
                int[] c = getColorsBits();
                String extension = "txt";

                String backgroundFilePath = window.tKSciezkaDoKodowania.getText();

                String outFilePath = window.tKSciezkaDoKodowaniaNowego.getText()
                        + File.separator
                        + window.tKNazwaNowegoPliku.getText();

                if (window.rKBin.isSelected()) //do zmiany. zmienic kontruktory
                {
                    String plikUkrywanyPath = window.tKSciezkaIn.getText();
                    File f = new File(plikUkrywanyPath);

                    int i = f.getName().lastIndexOf('.');
                    if (i > 0)
                    {
                        extension = f.getName().substring(i + 1);
                    }

                    FileInputStream hiddenFile = new FileInputStream(f);

                    data = new byte[hiddenFile.available()];
                    hiddenFile.read(data);
                    hiddenFile.close();

                }
                else
                {
                    data = window.tKText.getText().getBytes();
                }

                encrypter = new BinEncrypter(data, extension, backgroundFilePath, outFilePath, c[0], c[1], c[2]);

                if (encrypter.isEncryptionPossible())
                {
                    encrypter.encryptData();
                    JOptionPane.showMessageDialog(window, "Ukrywanie danych powiodło się.");
                }
                else
                {
                    JOptionPane.showMessageDialog(window, "Kodowanie niemożliwe. Za mało miejsca.");
                }
                window.addChart(window.tKNazwaNowegoPliku.getText(), (int) encrypter.getMaxDataLength() - data.length, data.length);
                encrypter.closeStreams();
            }
            catch (IOException io)
            {
                JOptionPane.showMessageDialog(window, "Błąd kodowania plików");
            }

        }

    }

    //////////////////// Dekodowanie
    class BDPrzegladajPlikWejsciowyListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent ae)
        {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("."));
            chooser.setFileFilter(new FileNameExtensionFilter("Pliki bmp", "bmp"));
            chooser.setAcceptAllFileFilterUsed(false); //wylaczenie wszystkich plikow
            int result = chooser.showDialog(window, "Wybierz plik");

            if (result == JFileChooser.APPROVE_OPTION)
            {
                window.tDSciezkaPlikWejsciowy.setText(chooser.getSelectedFile().getPath());
            }
        }
    }

    class BDSprawdzListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent ae)
        {
            try
            {
                String pathIn = window.tDSciezkaPlikWejsciowy.getText();

                tester = new BinTester(pathIn);

                if (tester.isBinEncrypted())
                {
                    window.lDIleBajtow.setText("tak");
                    window.lDextension.setText(tester.getReader().getImage().getDataHeader().getFileExtension());
                    decrypterFileExtension = tester.getReader().getImage().getDataHeader().getFileExtension();
                }
                else
                {
                    window.lDIleBajtow.setText("nie");
                }
                tester.closeStreams();
            }
            catch (IOException ex)
            {
                JOptionPane.showMessageDialog(window, "Błąd sprawdzania.");
            }

        }
    }

    class BDPrzegladajOutListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent ae)
        {
            try
            {
                JFileChooser chooser = new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.setCurrentDirectory(new File(".").getCanonicalFile());
                int result = chooser.showDialog(window, "Wybierz katalog");

                if (result == JFileChooser.APPROVE_OPTION)
                {
                    window.tDSciezkaOut.setText(chooser.getSelectedFile().getPath());
                    window.tDNazwaOdkodowanegoPliku.setText("odkodowany." + decrypterFileExtension);
                }
            }
            catch (IOException ex)
            {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    class BDDekodujListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent ae)
        {
            try
            {
                String backgroundFilePath = window.tDSciezkaPlikWejsciowy.getText();

                if (window.rDPlik.isSelected())
                {
                    String onlyDataFilePath = window.tDSciezkaOut.getText()
                            + File.separator
                            + window.tDNazwaOdkodowanegoPliku.getText();
                    System.out.println(onlyDataFilePath);
                    decrypter = new BinDecrypter(backgroundFilePath, onlyDataFilePath);
                    decrypter.decryptAndWrite();

                }
                else
                {
                    decrypter = new BinDecrypter(backgroundFilePath);
                    window.tDTekst.setText(decrypter.decryptText());
                }

                JOptionPane.showMessageDialog(window, "Dekodowanie powiodło się.");

                decrypter.closeStreams();

            }
            catch (IOException ex)
            {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    private int[] getColorsBits()
    {
        int[] t = new int[3];
        t[0] = (int) window.sKNiebieski.getValue();
        t[1] = (int) window.sKZielony.getValue();
        t[2] = (int) window.sKCzerwony.getValue();

        return t;
    }
}

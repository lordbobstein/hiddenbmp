/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

import java.io.IOException;

/**
 * Klasa stworzona tylko do sprawdzania możliwości zakodowania
 *
 * @author Lord Bobstein
 */
public class BinTester extends BinProcessor
{

    //Do szyfrowania
    public BinTester(byte[] t, String hidddenFileExtension,String fileInPath, String fileOutPath, int blueBits, int greenBits, int redBits) throws IOException
    {
        super(t, hidddenFileExtension, fileInPath, fileOutPath, blueBits, greenBits, redBits);
    }

    
    /** Kontruktor umożliwiający sprawdzenie czy wskazane dane się zmieszczą.
     * 
     * @param t
     * @param fileInPath
     * @param blueBits
     * @param greenBits
     * @param redBits
     * @throws IOException 
     */
    public BinTester(byte[] t, String fileInPath, int blueBits, int greenBits, int redBits) throws IOException
    {
        super(t, fileInPath, blueBits, greenBits, redBits);
    }

    /** Kontruktor umożliwiający sprawdzenie czy są zakodowane jakieś dane
     * 
     * @param fileInPath
     */
    public BinTester(String fileInPath) throws IOException
    {
        super(fileInPath);
    }
    
    


    /**
     * Sprawdza czy możliwe jest zapisanie danych. Nie uwzględnia juz zapisanego
     *
     * @return true - jest możliwe zapisanie danych o danej długości false - nie
     */
    public boolean isEncryptionPossible()
    {
        
        
        int blueBits = image.getDataHeader().getBlueUsed();
        int greenBits = image.getDataHeader().getGreenUsed();
        int redBits = image.getDataHeader().getRedUsed();
        
        long size = ((blueBits + redBits + greenBits) * (image.calculateSize()/24)) ;
        
        if (size >= data.length)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Oblicza calkowitą ilość miejsca, na której można zapisać dane. Nie
     * uwzględnia danych już zapisanych. 
     *
     * @return ilość bajtów, które można zapisać w obrazku
     */
    public long getMaxDataLength()
    {
        int blueBits = image.getDataHeader().getBlueUsed();
        int greenBits = image.getDataHeader().getGreenUsed();
        int redBits = image.getDataHeader().getRedUsed();
        
        long size = ((blueBits + redBits + greenBits) * (image.calculateSize()/24)) ;

        return size ; 
    }

    /**
     * Oblicza maksymalną wielkość pliku, który chcemy ukryć
     *
     * @return maksymalna wielkość pliku możliwego do ukrycia
     */
    public long getMaxBytes()
    {
        long size = image.calculateSize();

        return size / 8; ///16 bo char jest 2 bajtowy

    }

}

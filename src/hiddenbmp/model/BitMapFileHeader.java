/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

/**
 *
 * @author Lord Bobstein
 */
public class BitMapFileHeader implements Cloneable
{

    /**
     * Identyfikator BMP. Najczesciej BM
     *
     */
    private short bfType;
    /**
     * Calkowita wielkosc pliku
     *
     */
    private int bfSize;
    /**
     * Nico. Przewaznie wartosc 0. Można wykorzystać do ukryca 16 bitow danych
     */
    private short bfReserved1;
    /**
     * Nico2. Przewaznie wartosc 0. Można wykorzystać do ukryca 16 bitow danych
     */
    private short bfReserved2;
    /**
     * Offset poczatku danych w pliku
     *
     */
    private int bfOffBits;

    //////////// KONSTRUKTORY /////////////////////////////////////////////////
    public BitMapFileHeader(short bfType, int bfSize, short bfReserved1, short bfReserved2, int bfOffBits)
    {
        this.bfType = bfType;
        this.bfSize = bfSize;
        this.bfReserved1 = bfReserved1;
        this.bfReserved2 = bfReserved2;
        this.bfOffBits = bfOffBits;
    }

    ////////////////////////////////////////////////////////////////////////////
    @Override
    public BitMapFileHeader clone() throws CloneNotSupportedException
    {
        return (BitMapFileHeader) super.clone();
    }

    //////////// METODY ///////////////////////////////////////////////////////
    @Override
    public String toString()
    {
        char[] bf = new char[2];
        bf[1] = (char) (bfType >> 8);
        bf[0] = (char) (bfType & 0xff);

        return "BitMapFileHeader{" + "bfType=" + bf[0] + bf[1] + ", bfSize=" + bfSize
                + ", bfReserved1=" + bfReserved1 + ", bfReserved2="
                + bfReserved2 + ", bfOffBits=" + bfOffBits + '}';
    }

    //////////// GETTERY I SETTERY ////////////////////////////////////////////
    public short getBfType()
    {
        return bfType;
    }

    public void setBfType(short bfType)
    {
        this.bfType = bfType;
    }

    public int getBfSize()
    {
        return bfSize;
    }

    public void setBfSize(int bfSize)
    {
        this.bfSize = bfSize;
    }

    public short getBfReserved1()
    {
        return bfReserved1;
    }

    public void setBfReserved1(short bfReserved1)
    {
        this.bfReserved1 = bfReserved1;
    }

    public short getBfReserved2()
    {
        return bfReserved2;
    }

    public void setBfReserved2(short bfReserved2)
    {
        this.bfReserved2 = bfReserved2;
    }

    public int getBfOffBits()
    {
        return bfOffBits;
    }

    public void setBfOffBits(int bfOffBits)
    {
        this.bfOffBits = bfOffBits;
    }
}

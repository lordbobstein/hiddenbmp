/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

/**
 *
 * @author Lord Bobstein
 */
public class BmpImage implements Cloneable
{

    public class ColorsQueue
    {

        //ile bitow na kazdy kanał
        private int blueBits;
        private int greenBits;
        private int redBits;
        private Colors color = Colors.BLUE;

        //w którym aktualnie miejscu bajta
        private int blueIndex = 0;
        private int greenIndex = 0;
        private int redIndex = 0;

        //Wskazuje na ktorym bajcie aktualnie kodujemy
        public int byteIndex = 0;

        public ColorsQueue()
        {
            this.blueBits = dataHeader.getBlueUsed();
            this.greenBits = dataHeader.getGreenUsed();
            this.redBits = dataHeader.getRedUsed();
        }

        /**
         * Wskazuje na ktorym obecnie bicie jestesmy w danym bajcie
         *
         * @return
         */
        public int getPointer()
        {
            int index = 0;

            switch (color)
            {
                case BLUE:
                    index = blueIndex;
                    break;
                case GREEN:
                    index = greenIndex;
                    break;
                case RED:
                    index = redIndex;
                    break;
            }

            return index;
        }

        /**
         * Przesuwa wskaznik bitu. W razie potrzeby zmienia aktywny kolor;
         *
         */
        public void movePointer()
        {

            if (getPointer() == (getLenght() - 1))
            {
                switch (color)
                {
                    case BLUE:
                        blueIndex = 0;
                        byteIndex++;
                        color = Colors.GREEN;
                        break;
                    case GREEN:
                        greenIndex = 0;
                        byteIndex++;
                        color = Colors.RED;
                        break;
                    case RED:
                        redIndex = 0;
                        byteIndex++;
                        color = Colors.BLUE;
                        break;
                }
            }
            else
            {
                switch (color)
                {
                    case BLUE:
                        blueIndex++;
                        break;
                    case GREEN:
                        greenIndex++;
                        break;
                    case RED:
                        redIndex++;
                        break;
                }

            }
        }

        /**
         * Zwraca aktualna dlugosc potrzebnych bitow dla danego koloru
         *
         * @return
         */
        public int getLenght()
        {
            int index = 0;

            switch (color)
            {
                case BLUE:
                    index = blueBits;
                    break;
                case GREEN:
                    index = greenBits;
                    break;
                case RED:
                    index = redBits;
                    break;
            }

            return index;
        }

    }

    private enum Colors
    {
        BLUE, GREEN, RED
    };
    private DataHeader dataHeader;
    private BitMapFileHeader bmpFileHeader;
    private BitMapInfoHeader bmpInfoHeader;
    private byte[] imageData;
    private boolean fileHeaderSet;
    private boolean infoHeaderSet;
    private boolean imageDataSet;

    @Override
    public String toString()
    {
        return bmpFileHeader.toString() + '\n' + bmpInfoHeader.toString() + '}';
    }

    @Override
    public BmpImage clone() throws CloneNotSupportedException
    {
        BmpImage image = (BmpImage) super.clone();

        image.bmpFileHeader = bmpFileHeader.clone();
        image.bmpInfoHeader = bmpInfoHeader.clone();

        return image;
    }

    public void printImageData()
    {
        int rowLenght = calculateRoundedWidth();
        int row = 0;

        if (!isImageCorrect())
        {
            System.out.println("WrongData");
        }

        System.out.println("Red Green Blue Piksel in row");

        for (int i = 0; i < imageData.length; i += 3)
        {
            if (i % rowLenght == 0)
            {
                System.out.println("Row " + (row + 1));
                row++;
            }

            BmpPixel pixel = new BmpPixel(imageData[i], imageData[i + 1], imageData[i + 2]);

            System.out.print(pixel.getIntRed() + " ");
            System.out.print(pixel.getIntGreen() + " ");
            System.out.print(pixel.getIntBlue() + " ");
            System.out.println("i: " + i);

        }

    }

    /**
     * Sprawdza czy obraz jest gotowy do użycia
     *
     * @return true- jest gotowy false - nie
     */
    public boolean isImageCorrect()
    {
        if (fileHeaderSet && infoHeaderSet && imageDataSet)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Zaokragla szerokosc do do najblizszej kolejnej wielokrotnosci 4 w górę
     *
     * @return
     */
    public int calculateRoundedWidth()
    {
        int width = getBmpInfoHeader().getBiWidth();
        int rest = width % 4;

        switch (rest)
        {
            case 1:
                width += 3;
                break;
            case 2:
                width += 2;
                break;
            case 3:
                width += 1;
                break;
        }

        return width;
    }

    /**
     * Oblicza całkowitą ilość potrzebnej pamięci w celu przechowywania w
     * pamięci
     *
     * @return long - ilość potrzebnej pamięci w bajtach
     */
    public long calculateSize()
    {
        return calculateRoundedWidth() * getBmpInfoHeader().getBiHeight()
                * (getBmpInfoHeader().getBiBitCount() / 8);
    }

    public BitMapFileHeader getBmpFileHeader()
    {
        return bmpFileHeader;
    }

    public void setBmpFileHeader(BitMapFileHeader bmpFileHeader)
    {
        this.bmpFileHeader = bmpFileHeader;
        fileHeaderSet = true;
    }

    public BitMapInfoHeader getBmpInfoHeader()
    {
        return bmpInfoHeader;
    }

    public void setBmpInfoHeader(BitMapInfoHeader bmpInfoHeader)
    {
        this.bmpInfoHeader = bmpInfoHeader;
        infoHeaderSet = true;
    }

    public byte[] getImageData()
    {
        return imageData;
    }

    public void setImageData(byte[] imageData)
    {
        this.imageData = imageData;
        imageDataSet = true;
    }

    public DataHeader getDataHeader()
    {
        return dataHeader;
    }

    public void setDataHeader(DataHeader dataHeader)
    {
        this.dataHeader = dataHeader;
    }

}

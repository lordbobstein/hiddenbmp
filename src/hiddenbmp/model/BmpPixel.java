/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

/**
 *
 * @author Lord Bobstein
 */
public class BmpPixel
{

    private byte red;
    private byte green;
    private byte blue;

    public BmpPixel(byte blue, byte green, byte red)
    {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public BmpPixel(byte[] colors)
    {
        this.blue = colors[0];
        this.green = colors[1];
        this.red = colors[2];
    }

    public byte getByteRed()
    {
        return red;
    }

    public int getIntRed()
    {
        return (int) red & 0xff;
    }

    public void setRed(byte red)
    {
        this.red = red;
    }

    public byte getByteGreen()
    {
        return green;
    }

    public int getIntGreen()
    {
        return (int) green & 0xff;
    }

    public void setGreen(byte green)
    {
        this.green = green;
    }

    public byte getByteBlue()
    {
        return blue;
    }

    public int getIntBlue()
    {
        return (int) blue & 0xff;
    }

    public void setBlue(byte blue)
    {
        this.blue = blue;
    }
}

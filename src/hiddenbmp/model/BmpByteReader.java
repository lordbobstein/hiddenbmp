/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author Lord Bobstein
 */
public class BmpByteReader
{

    private FileInputStream file;

    public BmpByteReader(FileInputStream file)
    {
        this.file = file;
    }

    /**
     * Czyta pojedynczy bajt
     *
     * @return przeczytany bajt
     */
    public byte readByte() throws IOException
    {
        byte[] b = new byte[1];
        file.read(b, 0, 1);

        return b[0];
    }

    /**
     * Czyta określoną ilość bajtów
     *
     * @param len ilość bajtów do przeczytania
     * @return tablicę zwierającą przeczytane bajty
     */
    public byte[] readBytes(int len) throws IOException
    {
        byte[] b = new byte[len];
        file.read(b, 0, len);

        return b;
    }

    /**
     * Czyta wszystko i próbuje upchać w tablicy o wielkości len. Trochę bez
     * sensu
     *
     * @param len wielkość tablicy
     * @return tablicę zwierającą przeczytane bajty
     */
    public byte[] readAllBytes(int len) throws IOException
    {
        byte[] b = new byte[len];
        file.read(b);

        return b;
    }

    /**
     * Czyta cały wiersz. Stosować tylko do danych dotyczących zapisu pikseli w
     * RGB
     *
     * @param width szerokość wiersza
     * @return przeczytany wiersz
     */
    public byte[] readRow(int width) throws IOException
    {
        byte[] b = new byte[width * 3];
        file.read(b, 0, width * 3);

        return b;
    }

    /**
     * Czyta bajt i przekształca w char
     *
     * @return zwraca znak
     */
    public char readChar() throws IOException
    {
        byte[] b = new byte[1];
        //b- nazwa tablicy, 0-miejsce w tablicy 1- ile bajtow
        file.read(b, 0, 1);

        return (char) b[0];
    }

    /**
     * Czyta bajty i przekształca w tablice charów
     *
     * @param len ile bajtów przeczytać
     * @return tablicę charów
     */
    public char[] readChars(int len) throws IOException
    {
        byte[] b = new byte[len];
        //b- nazwa tablicy, 0-miejsce w tablicy 1- ile bajtow
        file.read(b, 0, len);

        char[] chars = new char[len];
        for (int i = 0; i < len; i++)
        {
            chars[i] = (char) b[i];
        }

        return chars;
    }

    //////////////////////////////////////////////////////////////////////////
    /**
     * Czyta 4 bajty (DWORD) i przekształca w int. DWORD to odpowiednik unsigned
     * int. Unsigned nie występuje w Javie
     *
     * @return unsigned int
     */
    public int readDWORD() throws IOException
    {
        byte[] b = new byte[4]; //dword - 32 bity
        file.read(b, 0, 4);

        int i = 0;
        i += ((int) b[3] & 0xff) << 24;
        i += ((int) b[2] & 0xff) << 16;
        i += ((int) b[1] & 0xff) << 8;
        i += ((int) b[0] & 0xff) << 0;

        return i;
    }

    ////////////////////////////////////////////////////////
    /**
     * Czyta 2 bajty (WORD) i przekształca w int. WORD to odpowiednik unsigned
     * short. Unsigned nie występuje w Javie
     *
     * @return unsigned short
     */
    public short readWORD() throws IOException
    {
        byte[] b = new byte[2]; //word - 16 bitow
        file.read(b, 0, 2);

        short i = 0;

        i += ((short) b[1] & 0xff) << 8;
        i += ((short) b[0] & 0xff) << 0;

        return i;
    }

    /**
     * Czyta 4 bajty (LONG) i przekształca w int. LONG to odpowiednik int.
     *
     * @return int
     */
    public int readLONG() throws IOException
    {
        byte[] b = new byte[4];
        file.read(b, 0, 4);

        int i = 0;
        i += (b[3] & 0xff) << 24;
        i += (b[2] & 0xff) << 16;
        i += (b[1] & 0xff) << 8;
        i += (b[0] & 0xff) << 0;

        return i;
    }
}

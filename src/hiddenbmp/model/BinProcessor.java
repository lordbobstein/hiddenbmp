/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Lord Bobstein
 */
abstract class BinProcessor
{
    protected byte[] data;
    protected String text;
    protected BmpImage image;
    protected BmpReader reader;
    protected BmpWriter writer;
    protected FileInputStream fileIn;
    
    private  FileOutputStream fileOut;

    /**
     * Kontruktor dla encryptera. Wersja binarna.
     *
     * @param t tablica bajtów do zaszyfrowania
     * @param fileIn plik "tło", z którego będa pobierane dane
     * @param fileOut plik wyjściowy już z zaszyfrowanymi danymi
     */
    public BinProcessor(byte[] t, String hidddenFileExtension,String fileInPath, String fileOutPath, int blueBits, int greenBits, int redBits) throws IOException
    {
        this.data = t;
        this.fileIn = new FileInputStream(new File(fileInPath));
        this.reader = new BmpReader(fileIn);
        this.readImage();
        DataHeader dataHeader = new DataHeader(hidddenFileExtension, (byte) blueBits, (byte)greenBits, (byte)redBits);
        image.setDataHeader(dataHeader);

        fileOut = new FileOutputStream(new File(fileOutPath));
        this.writer = new BmpWriter(image,fileOut);
    }

    /**
     * Kontruktor dla testera. Wersja binarna.
     *
     * @param t tablica bajtów
     * @param fileIn plik "tło", z którego będa pobierane dane
     */
    public BinProcessor(byte[] t, String fileInPath, int blueBits, int greenBits, int redBits) throws IOException
    {
        this.fileIn = new FileInputStream(new File(fileInPath));
        this.reader = new BmpReader(fileIn);     
        this.readImage();
        DataHeader dataHeader = new DataHeader((byte) blueBits, (byte)greenBits, (byte)redBits);
        image.setDataHeader(dataHeader);
        this.data = t;
    }    

    /**
     * Kontruktor dla decrytera.
     *
     * @param fileIn Plik czytany, w którym ukryte są dane.
     * @param fileOut Plik wyjściowy zawierający same dane.
     */
    public BinProcessor(String fileInPath, String fileOutPath) throws IOException
    {
        this.fileIn = new FileInputStream(new File(fileInPath));
        this.reader = new BmpReader(fileIn);
        this.readImage();       

        fileOut = new FileOutputStream(new File(fileOutPath));
        this.writer = new BmpWriter(image, fileOut);
    }

    /**
     * Konstruktor dla sprawdzenia czy są jakieś zakodowane dane
     *
     * @param fileIn
     * @throws IOException
     */
    public BinProcessor(String fileInPath) throws IOException
    {
        this.fileIn = new FileInputStream(fileInPath) ;
        this.reader = new BmpReader(fileIn);        
        this.readImage();
    }

    /**
     * 
     */
    private void readImage() throws IOException
    {
        image = this.reader.readHeaders();
        
        if (isBinEncrypted()) //jesli cos zaszyfrowane
        {
            image = this.reader.readDataHeaderAndImageData();
        }
        else
        {
            image = this.reader.readImageData();
        }
    }
    
    /**
     * Sprawdza czy w obrazie ukryta jest juz jakas informacja tekstowa
     *
     * @return true - jest false - nie
     */
    public boolean isBinEncrypted()
    {
        if (image.getBmpFileHeader().getBfReserved1() == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    ////////////////////
    public BmpReader getReader()
    {
        return reader;
    }

    public void setReader(BmpReader reader)
    {
        this.reader = reader;
    }

    public BmpWriter getWriter()
    {
        return writer;
    }

    public void setWriter(BmpWriter writer)
    {
        this.writer = writer;
    }
    
    public void closeStreams() throws IOException
    {
        
        fileIn.close();     
        
    }

}

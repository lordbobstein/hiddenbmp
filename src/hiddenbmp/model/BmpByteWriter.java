/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Lord Bobstein
 */
public class BmpByteWriter
{

    private FileOutputStream file;

    public BmpByteWriter(FileOutputStream file)
    {
        this.file = file;
    }

    /**
     * Zapisuje do pliku 1 bajt
     *
     * @param b bajt do zapisu
     */
    public void writeByte(byte b) throws IOException
    {
        file.write(b);
    }

    /**
     * Zapisuje do pliku tablicę bajtów
     *
     * @param b bajty do zapisania
     */
    public void writeBytes(byte[] b) throws IOException
    {
        file.write(b);
    }

    /**
     * Zapisuje do pliku liczbę typu WORD
     *
     * @param i liczba typu word, która zostanie zapisana
     */
    public void writeWORD(short i) throws IOException
    {
        byte[] b = new byte[2];

        b[0] = (byte) (i & 0xff);
        b[1] = (byte) (i >> 8);

        file.write(b);
    }

    /**
     * Zapisuje do pliku liczbę typu DWORD
     *
     * @param i liczba typu dword, która zostanie zapisana
     */
    public void writeDWORD(int i) throws IOException
    {
        byte[] b = new byte[4];

        b[0] = (byte) (i & 0xff);
        b[1] = (byte) ((i & 0xff00) >> 8);
        b[2] = (byte) ((i & 0xff0000) >> 16);
        b[3] = (byte) ((i & 0xff000000) >> 24);

        file.write(b);
    }

    /**
     * Zapisuje do pliku liczbę typu LONG
     *
     * @param i liczba typu long, która zostanie zapisana
     */
    public void writeLONG(int i) throws IOException
    {
        byte[] b = new byte[4];

        b[0] = (byte) (i & 0xff);
        b[1] = (byte) ((i & 0xff00) >> 8);
        b[2] = (byte) ((i & 0xff0000) >> 16);
        b[3] = (byte) ((i & 0xff000000) >> 24);

        file.write(b);
    }

}

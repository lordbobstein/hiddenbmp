/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

import java.io.IOException;

/**
 *
 * @author Lord Bobstein
 */
public class BinEncrypter extends BinTester
{

    public BinEncrypter(byte[] t, String hidddenFileExtension, String fileIn, String fileOut, int blueBits, int greenBits, int redBits) throws IOException
    {
        super(t, hidddenFileExtension, fileIn, fileOut, blueBits, greenBits, redBits);
    }

    class BitQueue
    {

        private int byteIndex = 0;
        private int bitIndex = 0;
        private byte b;

        public byte getNextBit()
        {
            //24 bo rzutowanie z inta
            b = (byte) ((data[byteIndex] << (7 - bitIndex + 24)) >>> (7 + 24));

            if (bitIndex == 7)
            {
                bitIndex = 0;
                byteIndex++;
            }
            else
            {
                bitIndex++;
            }

            return b;
        }

        public int getByteIndex()
        {
            return byteIndex;
        }

        public int getBitIndex()
        {
            return bitIndex;
        }

    }

    class Encrypter
    {

        private byte[] im = image.getImageData();

        BmpImage.ColorsQueue color = image.new ColorsQueue();

        public void setNextBit(byte b)
        {
            int byteIndex = color.byteIndex;

            if (color.getPointer() == 0)
            {
                int mask = 255;
                int d = 1;

                //obliczanie odpowiedniej maski
                for (int i = 0; i < color.getLenght(); i++)
                {
                    mask -= d;
                    d *= 2;
                }

                //wyczyszczenie odpowiedniej ilosci miejsc
                im[byteIndex] = (byte) (im[byteIndex] & mask);

            }

            if (b == 1)
            {
                int mask = 1;
                for (int i = 0; i < color.getPointer(); i++)
                {
                    mask *= 2;
                }

                im[byteIndex] = (byte) (im[byteIndex] | mask);

            }
            color.movePointer();

        }

    }

    public void encryptData() throws IOException
    {
        BitQueue bitQueue = new BitQueue();
        Encrypter encrypter = new Encrypter();
        byte b;
        image.getBmpFileHeader().setBfOffBits(54 + 8);
        int dataLenght = data.length; //długość danych do zakodowania

        for (int i = 0; i < dataLenght * 8; i++)
        {
            b = bitQueue.getNextBit();
            encrypter.setNextBit(b);
        }
        System.out.println(dataLenght);
        //zapisuje ilosc bajtow znakow ukrytych w obrazku

        image.getBmpFileHeader().setBfReserved1((short) ((dataLenght << 16) >>> 16));
        image.getBmpFileHeader().setBfReserved2((short) (dataLenght >>> 16));
        System.out.println("Kodowanie");
        System.out.println(image.getBmpFileHeader().getBfReserved1());
        System.out.println(image.getBmpFileHeader().getBfReserved2());
        writer.writeCompleteImage();

    }

    public void toS()
    {
        image.printImageData();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Lord Bobstein
 */
public class BinDecrypter extends BinProcessor
{

    private FileOutputStream binWriter;
    private String text;

    public BinDecrypter(String fileInPath, String fileOutPath) throws IOException
    {
        super(fileInPath);

        this.binWriter = new FileOutputStream(new File(fileOutPath));
    }

    public BinDecrypter(String fileInPath) throws IOException
    {
        super(fileInPath);
    }

    class BitQueue
    {

        private BmpImage.ColorsQueue color = image.new ColorsQueue();
        private byte[] im = image.getImageData();
        private byte b;

        public byte getNextBit()
        {
            //System.out.println("TU: " +im[color.byteIndex]);
            b = (byte) ((im[color.byteIndex] << (7 - color.getPointer()) + 24) >>> (7+24));          
            
            color.movePointer();

            return b;
        }
    }

    class Writer
    {

        private byte[] data;
        private byte[] im = image.getImageData();
        private int byteIndex = 0;
        private int bitIndex = 0;

        public Writer(byte[] data)
        {
            this.data = data;
        }

        public void setNextBit(byte b)
        {

            if (bitIndex == 0)
            {
                //wyczyszczenie odpowiedniej ilosci miejsc
                data[byteIndex] = 0;
            }

            if (b == 1)
            {
                int mask = 1;
                for (int i = 0; i < bitIndex; i++)
                {
                    mask *= 2;
                }

                data[byteIndex] = (byte) (data[byteIndex] | mask);
            }

            bitIndex++;

            if (bitIndex > 7)
            {
                bitIndex = 0;
                byteIndex++;
            }

        }

        public byte[] getData()
        {
            return data;
        }

    }

    private void decrypt()
    {        
        BitQueue bitQueue = new BitQueue();
        byte b;

        int length = 0; //dlugosc danych w bajtach
        length += image.getBmpFileHeader().getBfReserved1();
        length += image.getBmpFileHeader().getBfReserved2() << 16;
        System.out.println("Odkodowywanie");
        System.out.println(length);
        
        byte[] data = new byte[length];
        Writer wr = new Writer(data);

        for (int i = 0; i < length * 8; i++)
        {

            b = bitQueue.getNextBit();           
            wr.setNextBit(b);

        }

        this.data = data;
    }

    public void decryptAndWrite() throws IOException
    {
        decrypt();
        
        binWriter.write(data);        
    }

       public String decryptText()
    {
        decrypt();
        String decryptedText = new String(data);
        //System.out.println(data[0]);

        return decryptedText;
    }
   
    @Override
    public void closeStreams() throws IOException
    {
        super.closeStreams();
        binWriter.close();
    }

}

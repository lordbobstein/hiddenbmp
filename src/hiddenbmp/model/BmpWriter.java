/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Lord Bobstein
 */
public class BmpWriter
{

    private BmpByteWriter out;
    private FileOutputStream fileStreamOut;
    private BmpImage image;

    public BmpWriter(BmpImage image, FileOutputStream fileStreamOut) throws FileNotFoundException
    {
        this.image = image;
        this.fileStreamOut = fileStreamOut;        
        out = new BmpByteWriter(fileStreamOut);
    }

    /**
     * Zapisuje do pliku cały obrazek
     *
     */
    public void writeCompleteImage() throws IOException
    {
        this.writeBitMapFileHeader();
        this.writeBitMapInfoHeader();
        this.writeDataHeader();
        this.writeCompleteImageData();
    }

    /**
     * Zapisuje BitMapFileHeader do pliku BMP
     *
     */
    private void writeBitMapFileHeader() throws IOException
    {
        BitMapFileHeader header = image.getBmpFileHeader();

        out.writeWORD(header.getBfType());
        out.writeDWORD(header.getBfSize());
        out.writeWORD(header.getBfReserved1());
        out.writeWORD(header.getBfReserved2());
        out.writeDWORD(header.getBfOffBits());
    }

    /**
     * Zapisuje BitMapInfoHeader do pliku BMP
     *
     */
    private void writeBitMapInfoHeader() throws IOException
    {
        BitMapInfoHeader header = image.getBmpInfoHeader();

        out.writeDWORD(header.getBiSize());
        out.writeLONG(header.getBiWidth());
        out.writeLONG(header.getBiHeight());
        out.writeWORD(header.getBiPlanes());
        out.writeWORD(header.getBiBitCount());
        out.writeDWORD(header.getBiCompression());
        out.writeDWORD(header.getBiSizeImage());
        out.writeLONG(header.getBiXPelsPerMeter());
        out.writeLONG(header.getBiYPelsPerMeter());
        out.writeDWORD(header.getBiClrlUsed());
        out.writeDWORD(header.getBiClrlImportant());
    }

    /**
     * Zapisuje dane obrazu takie jak ilość danych na kanał i rozszerzenie pliku
     *
     */
    private void writeDataHeader() throws IOException
    {        
        fileStreamOut.write(image.getDataHeader().toBytes());
    }

    /**
     * Zapisuje do pliku dane obrazu
     *
     */
    private void writeCompleteImageData() throws IOException
    {

        fileStreamOut.write(image.getImageData());
    }

}

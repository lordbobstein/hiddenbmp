/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

/**
 *
 * @author Lord Bobstein
 */
public class BitMapInfoHeader implements Cloneable
{

    /**
     * Wielkość nagłówka. 28h
     *
     */
    private int biSize;
    /**
     * Szerokość bitmapy
     *
     */
    private int biWidth;
    /**
     * Wysokość bitmapy
     *
     */
    private int biHeight;
    /**
     * Ilość płaszczyzn. 1h
     *
     */
    private short biPlanes;
    /**
     * Ilość bitów na piksel. 18h
     *
     */
    private short biBitCount;
    /**
     * Rodzaj kompresji. BI _ RGB (0), BI _ RLE8 (1), BI _ RLE4 (2) oraz BI _
     * BITFIELDS (3)
     */
    private int biCompression;
    /**
     * Wielkość nieskopresowanej bitmapy w pamięci
     *
     */
    private int biSizeImage;
    /**
     * DPI poziome
     *
     */
    private int biXPelsPerMeter;
    /**
     * DPI pionowe
     *
     */
    private int biYPelsPerMeter;
    /**
     * Użyta ilość kolorów
     *
     */
    private int biClrlUsed;
    /**
     * Ilość ważnych kolorów
     *
     */
    private int biClrlImportant;

    //////////// KONSTRUKTORY /////////////////////////////////////////////////
    public BitMapInfoHeader(int biSize, int biWidth, int biHeight, short biPlanes, short biBitCount, int biCompression, int biSizeImage, int biXPelsPerMeter, int biYPelsPerMeter, int biClrUser, int biClrlImportant)
    {
        this.biSize = biSize;
        this.biWidth = biWidth;
        this.biHeight = biHeight;
        this.biPlanes = biPlanes;
        this.biBitCount = biBitCount;
        this.biCompression = biCompression;
        this.biSizeImage = biSizeImage;
        this.biXPelsPerMeter = biXPelsPerMeter;
        this.biYPelsPerMeter = biYPelsPerMeter;
        this.biClrlUsed = biClrUser;
        this.biClrlImportant = biClrlImportant;
    }

    ////////////////////////////////////////////////////////////////////////////
    @Override
    public BitMapInfoHeader clone() throws CloneNotSupportedException
    {
        return (BitMapInfoHeader) super.clone();
    }

    //////////// METODY ////////////////////////////////////////////////////////
    @Override
    public String toString()
    {
        return "BitMapInfoHeader{" + "biSize=" + biSize + ", biWidth=" + biWidth
                + ", biHeight=" + biHeight + ", biPlanes=" + biPlanes
                + ", biBitCount=" + biBitCount + ", biCompression="
                + biCompression + ", biSizeImage=" + biSizeImage
                + ", biXPelsPerMeter=" + biXPelsPerMeter
                + ", biYPelsPerMeter=" + biYPelsPerMeter + ", biClrlUsed="
                + biClrlUsed + ", biClrlImportant=" + biClrlImportant + '}';
    }

    //////////// GETTERY I SETTERY ////////////////////////////////////////////
    public int getBiSize()
    {
        return biSize;
    }

    public void setBiSize(int biSize)
    {
        this.biSize = biSize;
    }

    public int getBiWidth()
    {
        return biWidth;
    }

    public void setBiWidth(int biWidth)
    {
        this.biWidth = biWidth;
    }

    public int getBiHeight()
    {
        return biHeight;
    }

    public void setBiHeight(int biHeight)
    {
        this.biHeight = biHeight;
    }

    public short getBiPlanes()
    {
        return biPlanes;
    }

    public void setBiPlanes(short biPlanes)
    {
        this.biPlanes = biPlanes;
    }

    public short getBiBitCount()
    {
        return biBitCount;
    }

    public void setBiBitCount(short biBitCount)
    {
        this.biBitCount = biBitCount;
    }

    public int getBiCompression()
    {
        return biCompression;
    }

    public void setBiCompression(int biCompression)
    {
        this.biCompression = biCompression;
    }

    public int getBiSizeImage()
    {
        return biSizeImage;
    }

    public void setBiSizeImage(int biSizeImage)
    {
        this.biSizeImage = biSizeImage;
    }

    public int getBiXPelsPerMeter()
    {
        return biXPelsPerMeter;
    }

    public void setBiXPelsPerMeter(int biXPelsPerMeter)
    {
        this.biXPelsPerMeter = biXPelsPerMeter;
    }

    public int getBiYPelsPerMeter()
    {
        return biYPelsPerMeter;
    }

    public void setBiYPelsPerMeter(int biYPelsPerMeter)
    {
        this.biYPelsPerMeter = biYPelsPerMeter;
    }

    public int getBiClrlUsed()
    {
        return biClrlUsed;
    }

    public void setBiClrlUsed(int biClrlUsed)
    {
        this.biClrlUsed = biClrlUsed;
    }

    public int getBiClrlImportant()
    {
        return biClrlImportant;
    }

    public void setBiClrlImportant(int biClrlImportant)
    {
        this.biClrlImportant = biClrlImportant;
    }
}

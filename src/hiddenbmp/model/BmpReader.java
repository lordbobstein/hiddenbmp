/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author Lord Bobstein
 */
public class BmpReader
{

    private BmpByteReader bmpByteReader;
    private FileInputStream file;
    private BmpImage image;

    public BmpReader(FileInputStream file)
    {
        this.image = new BmpImage();
        this.file = file;
        bmpByteReader = new BmpByteReader(file);
    }

    public BmpImage readHeaders() throws IOException
    {
        this.readBitMapFileHeader();
        this.readBitMapInfoHeader();
        
        return image;
    }

    /**
     * Czyta cały obraz od początku do końca. DLa plików w których nie było
     * szyfrowania
     *
     * @return przeczytany obraz w obiekcie BmpImage
     */
    public BmpImage readImageData() throws IOException
    {        
        this.readCompleteImageData();

        return image;
    }

    /**
     * Czyta łącznie z nagłówiem danych. Dla plików w których jest coś ukryte.
     *
     * @return
     * @throws IOException
     */
    public BmpImage readDataHeaderAndImageData() throws IOException
    {        
        this.readDataHeader();
        this.readCompleteImageData();

        return image;
    }

    ///////// METODY PRYWATNE /////////////////////////////////////////////////
    /**
     * Czyta BitMapFileHeader z pliku BMP
     *
     */
    private void readBitMapFileHeader() throws IOException
    {
        short bfType = bmpByteReader.readWORD();
        int bfSize = bmpByteReader.readDWORD();
        short bfReserved1 = bmpByteReader.readWORD();
        short bfReserved2 = bmpByteReader.readWORD();
        int bfOffBits = bmpByteReader.readDWORD();

        image.setBmpFileHeader(new BitMapFileHeader(bfType, bfSize, bfReserved1, bfReserved2, bfOffBits));
    }

    /**
     * Czyta BitMapInfoHeader z pliku BMP
     *
     */
    private void readBitMapInfoHeader() throws IOException
    {
        int biSize = bmpByteReader.readDWORD();
        int biWidth = bmpByteReader.readLONG();
        int biHeight = bmpByteReader.readLONG();
        short biPlanes = bmpByteReader.readWORD();
        short biBitCount = bmpByteReader.readWORD();
        int biCompression = bmpByteReader.readDWORD();
        int biSizeImage = bmpByteReader.readDWORD();
        int biYPelsPerMeter = bmpByteReader.readLONG();
        int biXPelsPerMeter = bmpByteReader.readLONG();
        int biClrlUsed = bmpByteReader.readDWORD();
        int biClrlImportant = bmpByteReader.readDWORD();

        image.setBmpInfoHeader(new BitMapInfoHeader(biSize, biWidth, biHeight, biPlanes,
                biBitCount, biCompression, biSizeImage, biXPelsPerMeter,
                biYPelsPerMeter, biClrlUsed, biClrlImportant));
    }

    /**
     * Czyta dataHeader z pliku BMP
     *
     */
    private void readDataHeader() throws IOException
    {
        byte extensionLenght = (byte) file.read();
        byte[] ex = new byte[extensionLenght];
        file.read(ex, 0, extensionLenght);
        file.skip(4 - extensionLenght);

        byte blue = (byte) file.read();
        byte green = (byte) file.read();
        byte red = (byte) file.read();

        DataHeader data = new DataHeader(new String(ex), blue, green, red);
        image.setDataHeader(data);
    }

    /**
     * Czyta wszystkie dane obrazu dotyczące bezpośrednio pikseli
     *
     */
    private void readCompleteImageData() throws IOException
    {
        int width = image.calculateRoundedWidth();
        int height = image.getBmpInfoHeader().getBiHeight();

        // *3 bo na 1 pixel 3 kolory(bajty)
        int len = width * height * 3;

        //przeczytanie danych ukrytych w przesunięciu offsetu. 54 - nagłówki
//        int offset = image.getBmpFileHeader().getBfOffBits() - 54;
//        bmpByteReader.readBytes(offset);

        byte[] b = bmpByteReader.readAllBytes(len);

//        byte[] b = new byte[len];
//        for (int i = 0; i < len; i++)
//        {            
//            b[i] = bmpByteReader.readByte();            
//        }
        image.setImageData(b);
    }

    /**
     * Wypisuje wiersz, ale tylko piksele wyświetlanie, a nie dopelnienie do 4
     *
     */
    private void printRow() throws IOException
    {
        int rowLenght = image.calculateRoundedWidth();
        byte[] b = new byte[rowLenght];

        System.out.println("Red Green Blue Piksel");

        for (int i = 0; i < rowLenght; i++)
        {
            b = bmpByteReader.readBytes(3);
            BmpPixel pixel = new BmpPixel(b);

            if (i < image.getBmpInfoHeader().getBiWidth())
            {
                System.out.print(pixel.getIntRed() + " ");
                System.out.print(pixel.getIntGreen() + " ");
                System.out.print(pixel.getIntBlue() + " ");
                System.out.println("i: " + (i + 1));
            }
        }
    }

    /**
     * Wypisuje wiersz, ale tylko piksele wyświetlanie, a nie dopelnienie do 4
     *
     */
    private void printCompleteRow() throws IOException
    {
        int rowLenght = image.calculateRoundedWidth();
        byte[] b;

        System.out.println("Red Green Blue Piksel");

        for (int i = 0; i < rowLenght; i++)
        {
            b = bmpByteReader.readBytes(3);
            BmpPixel pixel = new BmpPixel(b);

            System.out.print(pixel.getIntRed() + " ");
            System.out.print(pixel.getIntGreen() + " ");
            System.out.print(pixel.getIntBlue() + " ");
            System.out.println("i: " + (i + 1));
        }

    }

    /**
     * Czyta (!!!) i wypisuje wszystkie dane obrazu dotyczące bezpośrednio
     * pikseli Tylko do tymczasowego podglądu pliku
     */
    private void printCompleteImageData() throws IOException
    {
        int rowLenght = image.calculateRoundedWidth();

        int imageHeight = image.getBmpInfoHeader().getBiHeight();
        byte[] b = new byte[rowLenght];

        System.out.println("Red Green Blue Piksel in row");

        for (int i = 0; i < imageHeight; i++)
        {
            System.out.println("Row " + (i + 1));
            for (int j = 0; j < rowLenght; j++)
            {
                b = bmpByteReader.readBytes(3);
                BmpPixel pixel = new BmpPixel(b);

                System.out.print(pixel.getIntRed() + " ");
                System.out.print(pixel.getIntGreen() + " ");
                System.out.print(pixel.getIntBlue() + " ");
                System.out.println("i: " + (j + 1));
            }
        }

    }

    //////////// GETTERY I SETTERY ////////////////////////////////////////////
    public BmpImage getImage()
    {
        return image;
    }

    public BmpImage getImageCopy() throws CloneNotSupportedException
    {
        return image.clone();
    }

}

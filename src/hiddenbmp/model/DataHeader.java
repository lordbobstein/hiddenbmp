/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hiddenbmp.model;

/**
 *
 * @author Lord Bobstein
 */
public class DataHeader
{

    /**
     * Przechowuje rozszerzenie zapisanego pliku
     *
     */
    private String fileExtension;

    /**
     * Przechowuje na ilu bajtach zapisane jest rozszerzenie
     *
     */
    private byte fileExtensionLenght;

    /**
     * Przechowują ile bitów dla każdego koloru zostało użyte
     *
     */
    private byte blueLenght;
    private byte greenLenght;
    private byte redLenght;

    /**
     * Na którym bicie się skończyło
     *
     */
//    private byte end;

    public DataHeader(String fileExtension, byte blueUsed, byte greenUsed, byte redUsed)
    {
        this.fileExtension = fileExtension;
        this.fileExtensionLenght = (byte) fileExtension.length();
        this.blueLenght = blueUsed;
        this.greenLenght = greenUsed;
        this.redLenght = redUsed;
        
    }

    public DataHeader(byte blueUsed, byte greenUsed, byte redUsed)
    {
        this.blueLenght = blueUsed;
        this.greenLenght = greenUsed;
        this.redLenght = redUsed;
    }
    
    

    /**
     * Zwraca cały nagłówek jako tablicę bajtów
     *
     * @return tablica bajtów
     */
    public byte[] toBytes()
    {
        fileExtensionLenght = (byte) fileExtension.getBytes().length;
        byte[] t = new byte[8];
        byte[] temp = fileExtension.getBytes();
        int i = 0;

        t[i++] = fileExtensionLenght;

        for (; i <= fileExtensionLenght; i++)
        {
            t[i] = temp[i - 1];
        }
        
        if (fileExtensionLenght == 3) t[i++] = 0;       
            

        t[i++] = blueLenght;
        t[i++] = greenLenght;
        t[i++] = redLenght;

        return t;
    }

    public String getFileExtension()
    {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension)
    {
        this.fileExtension = fileExtension;
        this.fileExtensionLenght = (byte) fileExtension.length();
    }

    public byte getFileExtensionLenght()
    {
        return fileExtensionLenght;
    }

    public void setFileExtensionLenght(byte fileExtensionLenght)
    {
        this.fileExtensionLenght = fileExtensionLenght;
    }

    public byte getBlueUsed()
    {
        return blueLenght;
    }

    public void setBlueUsed(byte blueUsed)
    {
        this.blueLenght = blueUsed;
    }

    public byte getGreenUsed()
    {
        return greenLenght;
    }

    public void setGreenUsed(byte greenUsed)
    {
        this.greenLenght = greenUsed;
    }

    public byte getRedUsed()
    {
        return redLenght;
    }

    public void setRedUsed(byte redUsed)
    {
        this.redLenght = redUsed;
    }

//    public byte getEnd()
//    {
//        return end;
//    }
//
//    public void setEnd(byte end)
//    {
//        this.end = end;
//    }
    
    

}
